# 支付宝当面付打赏要饭系统

#### 介绍
通过单文件版整合集成当面付源码
本系统无需数据库

#### 安装教程
上传网站目录解压即可


#### 使用说明
    lib是类库，无需修改
    使用前请在数据文件中配置好当面付公钥和私钥 lib/config.php
如有问题联系 www.lailiyun.com

## 感谢

开源项目意味着作者没有任何收入来源，仅凭个人空闲时间开发，如果您有经济条件，您可以赞助本项目的开发（下方收款码），如果您不想赞助，也请您点击上面的Star给一个星星，也是对我莫大的认同，感谢各位的支持。

![微信赞助](https://gitee.com/phpfaka/bolefaka_v3/raw/master/wx.png)![支付宝赞助](https://gitee.com/phpfaka/bolefaka_v3/raw/master/alipay.jpg)