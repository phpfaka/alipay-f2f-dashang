<?php include_once 'lib/data.php';
$file = 'lib/jilu.txt';
$content = file_get_contents($file);
$dashang = array_values(array_filter(array_unique(array_reverse(explode("\n", $content)))));
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<title><?php echo $pay_config['title'];?></title>
<meta name="keywords" content="<?php echo $pay_config['keywords'];?>">
<meta name="description" content="<?php echo $pay_config['describe'];?>">
<link rel="shortcut icon" href="https://q1.qlogo.cn/g?b=qq&nk=<?php echo $pay_config['qq'];?>&s=100">
<meta itemprop="image" content="https://q1.qlogo.cn/g?b=qq&nk=<?php echo $pay_config['qq'];?>&s=100">
<link rel="stylesheet" type="text/css" href="./public/style.css" />
<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.0.min.js"></script>
</head>
<body>
<div class="web">
	<div class="container">
		<!--头部信息-->
		<header class="header">
		    <div class="header-logo">
			    <div class="portrait"><img src="https://q1.qlogo.cn/g?b=qq&nk=<?php echo $pay_config['qq'];?>&s=640" alt="<?php echo $pay_config['title'];?>"></div>
				<div class="synopsis">
				    <a href="<?php echo $domain;?>" title="LOGO"><?php echo $pay_config['name'];?></a>
					<div class="des"><?php echo $pay_config['qqinfo'];?></div>
				</div>
			</div>
			<div class="header-default">
			    <form action="./query.php?action=pay" method="post">
				    <div class="group">
					    <label>大爷打赏</label>
						<input type="text" name="money" value="1" required>
					</div>
					<div class="group">
					    <label>大侠称呼</label>
						<input type="text" name="name" value="匿名" required>
					</div>
					<div class="group">
					    <label>大侠留言</label>
						<input type="text" name="des" value="加油 ♥" required>
					</div>
					<button type="submit" id="submit">立即打赏</button>
				</form>
			</div>
		</header>
		
		
	    <table width="100%" border="0" style="background-color: white;">
          <tbody><tr>
            <td>称呼</td>
            <td>金额</tdtd>
            <td>寄语</td>
            <td>时间</td>
          </tr>
          <?php
            for($i = 0; $i < count($dashang); $i ++) {
                $temp = $dashang[$i];
                $items = explode('----',$temp);
                echo '<tr style="border-bottom:1px solid #ddd;padding-bottom:2px;">
                        <td width="110">'.$items[0] . '</td>
                        <td width="100">￥' . $items[1] . '</td>
                        <td width="230">' . $items[2] . '</td>
                        <td width="180">'.$items[3].'</td>
                      </tr>';
            }
            ?>
          </tbody>
          </table>
	</div>
	<div class="footer">
	    <p><a href="http://beian.miit.gov.cn" target="_blank" rel="nofollow">京ICP备123456号</a>&nbsp;技术：<a href="https://www.lailiyun.com/" target="_blank">来利云</a></p>
		<p>Copyright © 2020 <a href="<?php echo $domain;?>"><?php echo $pay_config['name'];?></a></p>
	</div>
</div>
<script>
$('#submit').click(function (){
	var money = $("input[name=money]").val();
	var reg = /^[0-9]*[0-9](.[0-9]{0,2})?$/;
	if (!reg.test(money)||money == 0){
		alert("请输入正确赏金");
		return false;
	}
	if (money > 1000){
		alert("赏金不能大于1000噢");
		return false;
	}
	return true;
});
</script>
</body>
</html>